﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebBanHang.Models;
using WebBanHang.Repositories;

namespace WebBanHang.Controllers
{
	public class ProductController : Controller
	{

		private readonly IproductRepository _productRepository;
		private readonly IcategoryRepository _categoryRepository;

		public ProductController(IproductRepository productRepository, IcategoryRepository categoryRepository)
		{
			_productRepository = productRepository;
			_categoryRepository = categoryRepository;
		}

		#region ADD

		// Hiển thị danh sách sản phẩm
		public async Task<IActionResult> Index()
		{
			var products = await _productRepository.GetAllAsync();
			return View(products);
		}

		// Hiển thị form thêm sản phẩm mới
		public async Task<IActionResult> Add()
		{
			var categories = await _categoryRepository.GetAllAsync();
			ViewBag.Categories = new SelectList(categories, "Id", "Name");
			return View();
		}

		// Xử lý thêm sản phẩm mới// Xử lý thêm sản phẩm mới
		[HttpPost]
		public async Task<IActionResult> Add(Product product, IFormFile imageUrl)
		{
			if (ModelState.IsValid)
			{
				if (imageUrl != null)
                {
                    if (ValidateImageExtension(imageUrl.FileName))
                    {
                        if (!ValidatImageSize(imageUrl, 1048576))
                        {
                            ModelState.AddModelError("ImageUrl", "Image size is too big. The limit is only 1MB");
                            return View(product);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("ImageUrl", "Invalid image format for main image. Please upload a jpg, jpeg, jfif, or png file.");
                        return View(product);
                    }
                    // Lưu hình ảnh đại diện tham khảo bài 02 hàm SaveImage
                    product.ImageUrl = await SaveImage(imageUrl);
				}

				await _productRepository.AddAsync(product);
				return RedirectToAction(nameof(Index));
			}

			// Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập
			var categories = await _categoryRepository.GetAllAsync();
			ViewBag.Categories = new SelectList(categories, "Id", "Name");

			return View(product);
		}
		#endregion

		#region AUD
		public async Task<IActionResult> Display(int id)
		{
			var product = await _productRepository.GetByIdAsync(id);
			if (product == null)
			{
				return NotFound();
			}
			return View(product);
		}

		// Hiển thị form cập nhật sản phẩm
		public async Task<IActionResult> Update(int id)
		{
			var product = await _productRepository.GetByIdAsync(id);
			if (product == null)
			{
				return NotFound();
			}
			var categories = await _categoryRepository.GetAllAsync();
			ViewBag.Categories = new SelectList(categories, "Id", "Name",

			product.CategoryId);

			return View(product);
		}

		// Xử lý cập nhật sản phẩm
		[HttpPost]
		public async Task<IActionResult> Update(int id, Product product,

		IFormFile imageUrl)

		{
			ModelState.Remove("ImageUrl"); // Loại bỏ xác thực ModelState cho ImageUrl
		
			if (id != product.Id)
			{
				return NotFound();
			}
			if (ModelState.IsValid)
			{
				var existingProduct = await

				_productRepository.GetByIdAsync(id); // Giả định có phương thức GetByIdAsync
				// Giữ nguyên thông tin hình ảnh nếu không có hình mới được tải lên
			
				if (imageUrl == null)
				{
					product.ImageUrl = existingProduct.ImageUrl;
				}
				else
				{
					// Lưu hình ảnh mới
					product.ImageUrl = await SaveImage(imageUrl);
				}
				// Cập nhật các thông tin khác của sản phẩm

				existingProduct.Name = product.Name;
				existingProduct.Price = product.Price;
				existingProduct.Description = product.Description;
				existingProduct.CategoryId = product.CategoryId;
				existingProduct.ImageUrl = product.ImageUrl;
				await _productRepository.UpdateAsync(existingProduct);
				return RedirectToAction(nameof(Index));
			}

			var categories = await _categoryRepository.GetAllAsync();
			ViewBag.Categories = new SelectList(categories, "Id", "Name");
			return View(product);
		}

		// Hiển thị form xác nhận xóa sản phẩm
		public async Task<IActionResult> Delete(int id)
		{
			var product = await _productRepository.GetByIdAsync(id);
			if (product == null)
			{
				return NotFound();
			}
			return View(product);
		}

		// Xử lý xóa sản phẩm
		[HttpPost, ActionName("DeleteConfirmed")]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			await _productRepository.DeleteAsync(id);
			return RedirectToAction(nameof(Index));
		}

		#endregion

		//Code xử lý hình ảnh
		#region images

		private bool ValidateImageExtension(string fileName)
		{
			var allowedExtensions = new string[] { ".jpg", ".jpeg", ".png", ".jfif" };
			return allowedExtensions.Contains(Path.GetExtension(fileName).ToLower());
		}

		private bool ValidatImageSize(IFormFile file, long maximumSize)
		{
			return file.Length <= maximumSize;
		}

		private async Task<string> SaveImage(IFormFile image)
		{
			var savePath = Path.Combine("wwwroot/images", image.FileName);

			using (var fileStream = new FileStream(savePath, FileMode.Create))
			{
				await image.CopyToAsync(fileStream);
			}
			return "/images/" + image.FileName; // Trả về đường dẫn tương đối
		}

		#endregion


	}
}
